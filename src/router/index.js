import { createRouter, createWebHistory } from "vue-router";
// import Mensajeria from '../views/MENSAJERIA/mensajeriaView.vue'
import IngresarAlumno from '../views/ADM_CURSOS/IngresarAlumno.vue'
import EliminarAlumno from '../views/ADM_CURSOS/EliminarAlumno.vue'
import IngresarDocenteJefe from '../views/ADM_CURSOS/IngresarDocenteJefe.vue'
import IngresarAsignatura from '../views/ADM_CURSOS/IngresarAsignatura.vue'
import CambiarNombreAsignatura from '../views/ADM_CURSOS/CambiarNombreAsignatura.vue'
import EliminarDocenteJefe from '../views/ADM_CURSOS/EliminarDocenteJefe.vue'
import EliminarAsignatura from '../views/ADM_CURSOS/EliminarAsignatura.vue'
import IngresarAsignaturaDocente from '../views/ADM_CURSOS/IngresarAsigDocente.vue'
import IngresarCurso from '../views/ADM_CURSOS/IngresarCurso.vue'
import HomeAdministracion from '../views/ADM_CURSOS/homeAdministracion.vue'
import login from '../views/CUENTAS/Login.vue'
import userMain from '../views/CUENTAS/userMain.vue'
import adminMain from '../views/CUENTAS/AdminMain.vue'
import perfil from '../views/CUENTAS/perfil.vue'
import nuevoUsuario from '../views/CUENTAS/AdminNewUser.vue'
import editarUsuario from '../views/CUENTAS/AdminEditUser.vue'
import asignarApoderado from '../views/CUENTAS/AdminAsignApoderado.vue'
import mensajeria from '../views/MENSAJERIA/mensajeriaView.vue'
import Horario from '../views/HORARIO/Horario.vue'
import EdicionHorario from '../views/HORARIO/EdicionHorario.vue'
import CrearTarea from '../views/Tareas/CrearTarea.vue';
import ElegirAsignatura from '../views/Tareas/ElegirAsignatura.vue';
import SubirTarea from '../views/Tareas/SubirTarea.vue';
import ElegirCurso from '../views/Tareas/ElegirCurso.vue';
import ElegirAsignaturaAlumno from '../views/Tareas/ElegirAsignaturaAlumno.vue';
import ListaTareasDocente from '../views/Tareas/ListaTareasDocente.vue';
import SeleccionarMateria from '../views/Asistencia/SeleccionarMateria.vue';
import RegistrarAsistencia from '../views/Asistencia/RegistrarAsistencia.vue';
import RegistroExitoso from '../views/Asistencia/RegistroExitoso.vue';
import VerRegistroAsistencia from '../views/Asistencia/VerRegistroAsistencia.vue';
import MainAsistenciaDocente from '../views/Asistencia/MainAsistenciaDocente.vue';
import MainAsistenciaEstudiante from '../views/Asistencia/MainAsistenciaEstudiante.vue';
import ModificarAsistEstudiante from '../views/Asistencia/ModificarAsistEstudiante.vue';
import DetalleTareaDocente from '../views/Tareas/DetalleTareaDocente.vue';
import DetalleTareaEstudiante from '../views/Tareas/DetalleTareaEstudiante.vue';
import ListaTareasEstudiante from '../views/Tareas/ListaTareasEstudiante.vue';
import ModificarTarea from '../views/Tareas/ModificarTarea.vue';
import DetalleEntrega from '../views/Tareas/DetalleEntrega.vue';
import ListaEntregasDocente from '../views/Tareas/ListaEntregasDocente.vue';


const routes = [
  {},
  {
    path: "/ingresarAlumno/:id",
    component: IngresarAlumno,
  },
  {
    path: "/eliminarAlumno/:id",
    component: EliminarAlumno,
  },
  {
    path: "/ingresarDocenteJefe/:id",
    component: IngresarDocenteJefe,
  },
  {
    path: "/ingresarAsignatura/:id",
    component: IngresarAsignatura,
  },
  {
    path: "/cambiarNombreAsignatura/:id",
    component: CambiarNombreAsignatura,
  },
  {
    path: "/eliminarDocenteJefe/:id",
    component: EliminarDocenteJefe,
  },
  {
    path: "/eliminarAsignatura/:id",
    component: EliminarAsignatura,
  },
  {
    path: "/ingresarAsignaturaDocente/:id",
    component: IngresarAsignaturaDocente,
  },
  {
    path: "/ingresarCurso",
    component: IngresarCurso,
  },
  {
    path: "/homeAdministracion/:id",
    component: HomeAdministracion,
  },
  {
    path: "/login",
    component: login,
  },
  {
    path: "/",
    component: userMain,
  },
  {
    path: "/admin",
    component: adminMain,
  },
  {
    path: "/admin/nuevo",
    component: nuevoUsuario,
  },
  {
    path: "/perfil",
    component: perfil,
  },
  {
    path: "/admin/editar/:id",
    component: editarUsuario,
    props: true,
  },
  {
    path: "/admin/asignarApoderado/:id",
    component: asignarApoderado,
    props: true,
  },
  {
  path: '/horario',
  component: Horario

  },
  {
  path: '/edicionHorario',
  component: EdicionHorario

  },
  {
    path: "/mensajeria",
    component: mensajeria,
  },
  {
    path: "/graficos",
    component: () => import("../views/REPORTES/GraficosNotasPage.vue"),
  },
  {
    path: "/reporteAsistencia",
    component: () => import("../views/REPORTES/AsistenciaPage.vue"),
  },
  {
    path: "/reporteNotas",
    component: () => import("../views/REPORTES/NotasPage.vue"),
  },
  {
    path: "/anotacionesPrueba",
    component: () => import("../views/REPORTES/AnotacionesPrueba.vue"),
  },
  {
    path: '/ElegirAsignatura',
    component: ElegirAsignatura
},
{
path:'/CrearTarea',
component:CrearTarea
},
{
path: '/SubirTarea',
component: SubirTarea
},
{
path: '/ElegirCurso',
component: ElegirCurso
}
,
{
path: '/ListaTareasDocente',
component: ListaTareasDocente
},
{
path: '/ListaTareasEstudiante',
component: ListaTareasEstudiante
},
{
path: '/SeleccionarMateria',
component: SeleccionarMateria
},
{
path: '/RegistrarAsistencia',
component: RegistrarAsistencia
},
{
path: '/RegistroExitoso',
component: RegistroExitoso
},

{
path: '/VerRegistroAsistencia',
component: VerRegistroAsistencia
},
{
path: '/MainAsistenciaDocente',
component: MainAsistenciaDocente
},
{
path: '/MainAsistenciaEstudiante',
component: MainAsistenciaEstudiante
},



{
path: '/DetalleTareaDocente',
component: DetalleTareaDocente

},

{
path: '/ModificarTarea',
component: ModificarTarea

},
{
path: '/DetalleEntrega/',
component: DetalleEntrega

},
{
path: '/ListaEntregasDocente/',
component: ListaEntregasDocente

},
{
path: '/DetalleTareaEstudiante',
component: DetalleTareaEstudiante

},
{
path: '/ModificarAsistEstudiante',
component: ModificarAsistEstudiante

},
{
path: '/ElegirAsignaturaAlumno',
component: ElegirAsignaturaAlumno

}
,


]

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
