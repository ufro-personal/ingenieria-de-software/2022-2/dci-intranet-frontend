import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import dns from "dns";

dns.setDefaultResultOrder("verbatim");

export default defineConfig({
  server: {
    port: 5173,
    hmr: { overlay: false }
  },
  resolve: {
    extensions: [
      ".mjs",
      ".js",
      ".ts",
      ".jsx",
      ".tsx",
      ".json",
      ".vue",
      ".scss",
    ],
  },
  plugins: [vue()],
});
